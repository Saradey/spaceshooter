package com.evgeny.gocnahrov.spaceshooter.common

import com.badlogic.gdx.Gdx
import com.evgeny.gocnahrov.spaceshooter.consts.DEBUG
import javax.inject.Inject

class Logger @Inject constructor() : ILogger {

    companion object {
        private const val TAG_DEBUG_SHOW = "TAG_DEBUG_SHOW"
    }

    override fun logD(message: String) {
        if (DEBUG) {
            Gdx.app.log(TAG_DEBUG_SHOW, message)
        }
    }


}
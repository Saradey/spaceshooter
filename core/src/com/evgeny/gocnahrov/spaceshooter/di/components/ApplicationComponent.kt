package com.evgeny.gocnahrov.spaceshooter.di.components

import com.evgeny.gocnahrov.spaceshooter.App
import com.evgeny.gocnahrov.spaceshooter.di.modules.ApplicationBindsModule
import com.evgeny.gocnahrov.spaceshooter.di.modules.ApplicationProvidesModule
import com.evgeny.gocnahrov.spaceshooter.di.scope.ApplicationScope
import com.evgeny.gocnahrov.spaceshooter.managers.navigation.ISwitcherScreen
import com.evgeny.gocnahrov.spaceshooter.managers.resource.ResourceImageGameManager
import com.evgeny.gocnahrov.spaceshooter.screens.game.GameScreen
import dagger.BindsInstance
import dagger.Component


@ApplicationScope
@Component(modules = [
    ApplicationProvidesModule::class,
    ApplicationBindsModule::class
])
interface ApplicationComponent {
    companion object {
        lateinit var component: ApplicationComponent

        fun init(app: App, switcherScreen: ISwitcherScreen): ApplicationComponent {
            component = DaggerApplicationComponent.factory()
                    .create(app, switcherScreen)
            return component
        }
    }
    @Component.Factory
    interface ApplicationComponentFactory {
        fun create(@BindsInstance app: App,
                   @BindsInstance switcherScreen: ISwitcherScreen): ApplicationComponent
    }

    fun inject(app: App)

    fun inject(gameScreen: GameScreen)

    fun inject(resourceImageGameManager: ResourceImageGameManager)
}
package com.evgeny.gocnahrov.spaceshooter.di.modules

import com.evgeny.gocnahrov.spaceshooter.di.scope.ApplicationScope
import com.evgeny.gocnahrov.spaceshooter.managers.resource.ResourceImageGameManager
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import dagger.Module
import dagger.Provides

@Module
class ApplicationProvidesModule {

    @Provides
    @ApplicationScope
    fun provideResourceManager() = ResourceImageGameManager()

    @Provides
    @ApplicationScope
    fun provideWindowManager() = WindowManager()

}
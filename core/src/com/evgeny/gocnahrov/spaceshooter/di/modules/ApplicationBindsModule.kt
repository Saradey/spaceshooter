package com.evgeny.gocnahrov.spaceshooter.di.modules

import com.evgeny.gocnahrov.spaceshooter.`object`.actions.playerAndBonus.ActionPlayerAndBonusImpl
import com.evgeny.gocnahrov.spaceshooter.`object`.actions.playerAndBonus.IActionPlayerAndBonus
import com.evgeny.gocnahrov.spaceshooter.`object`.actions.base.IMainAction
import com.evgeny.gocnahrov.spaceshooter.`object`.actions.base.MainActionImpl
import com.evgeny.gocnahrov.spaceshooter.common.ILogger
import com.evgeny.gocnahrov.spaceshooter.common.Logger
import com.evgeny.gocnahrov.spaceshooter.di.scope.ApplicationScope
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.BackgroundPresenterImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.IBackgroundPresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.BonusTypePresenterImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.IBonusTypePresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.bullet.BulletPlayerPresenterImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.bullet.IBulletPlayerPresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.GameObjectManagerImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init.IInitGameObject
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init.InitGameObjectManagerImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.IWaveManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.WaveManagerImpl
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.IPlayerPresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.PlayerPresenterImpl
import com.evgeny.gocnahrov.spaceshooter.managers.level.ILevelManager
import com.evgeny.gocnahrov.spaceshooter.managers.level.LevelManagerImpl
import com.evgeny.gocnahrov.spaceshooter.managers.navigation.INavigationManager
import com.evgeny.gocnahrov.spaceshooter.managers.navigation.NavigationManagerImpl
import com.evgeny.gocnahrov.spaceshooter.screens.game.render.GameRenderImpl
import com.evgeny.gocnahrov.spaceshooter.screens.game.world.GameWorldImpl
import com.evgeny.gocnahrov.spaceshooter.screens.game.render.IGameRender
import com.evgeny.gocnahrov.spaceshooter.screens.game.world.IGameWorld
import dagger.Binds
import dagger.Module

@Module
interface ApplicationBindsModule {

    @Binds
    @ApplicationScope
    fun bindNavigationManager(navManager: NavigationManagerImpl): INavigationManager

    @Binds
    fun bindGameWorld(gameWorld: GameWorldImpl): IGameWorld

    @Binds
    fun bindGameRender(gameRender: GameRenderImpl): IGameRender

    @Binds
    @ApplicationScope
    fun bindGameObjectManager(gameManager: GameObjectManagerImpl): IGameObjectManager

    @Binds
    fun bindInitGameObjectManager(init: InitGameObjectManagerImpl): IInitGameObject

    @Binds
    fun bindBackgroundPresenterImpl(presenter: BackgroundPresenterImpl): IBackgroundPresenter

    @Binds
    @ApplicationScope
    fun bindLogger(logger: Logger): ILogger

    @Binds
    fun bindPlayerPresenter(presenter: PlayerPresenterImpl): IPlayerPresenter

    @Binds
    fun bindLevelManager(manager: LevelManagerImpl): ILevelManager

    @Binds
    fun bindBonusTypePresenter(presenter: BonusTypePresenterImpl): IBonusTypePresenter

    @Binds
    fun bindActionPlayerAndBonus(action: ActionPlayerAndBonusImpl): IActionPlayerAndBonus

    @Binds
    fun bindMainAction(mainAction: MainActionImpl): IMainAction

    @Binds
    fun bindBulletPlayerPresenter(presenter: BulletPlayerPresenterImpl): IBulletPlayerPresenter

    @Binds
    @ApplicationScope
    fun bindWaveManager(wave: WaveManagerImpl): IWaveManager

}
package com.evgeny.gocnahrov.spaceshooter.game.objects.bullet

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.common.ILogger
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.Player
import com.evgeny.gocnahrov.spaceshooter.managers.resource.ResourceImageGameManager
import javax.inject.Inject

class BulletPlayerPresenterImpl @Inject constructor(
        private val gameObjectManager: IGameObjectManager,
        private val logger: ILogger,
        private val resource: ResourceImageGameManager
) : IBulletPlayerPresenter {

    companion object {
        const val FIRST_LVL_WEAPSON = 1
        const val SECOND_LVL_WEAPSON = 2
        const val THIRD_LVL_WEAPSON = 3
        const val FOURTH_LVL_WEAPSON = 4
        const val FIFTH_LVL_WEAPSON = 5
    }

    private var shootValue = 0f
    private var lastShoot = 1f

    private val player: Player by lazy {
        gameObjectManager.getPlayer()
    }

    private val allObj = gameObjectManager.getAllGameObject()


    override fun update(delta: Float) {
        shootValue += delta
        if (lastShoot < shootValue) {
            shootPlayer()
            lastShoot += player.fasterShoot
        }
        updateBullet(delta)
    }


    private fun shootPlayer() {
        when (player.weapsonLvl) {
            FIRST_LVL_WEAPSON -> initPlayerFirstWeapsonBullet()
            SECOND_LVL_WEAPSON -> initPlayerSecondWeapsonBullet()
            THIRD_LVL_WEAPSON -> initPlayerThirdWeaponBullet()
            FOURTH_LVL_WEAPSON -> initPlayerFourthWeapsonBullet()
            FIFTH_LVL_WEAPSON -> initPlayerFifthWeapsonBullet()
        }
    }


    private fun initPlayerFirstWeapsonBullet() {
        val bullet = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.centerWidth,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bullet)
    }


    private fun initPlayerSecondWeapsonBullet() {
        val bulletFirst = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 40,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFirst)
        val bulletSecond = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 60,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletSecond)
    }


    private fun initPlayerThirdWeaponBullet() {
        val bulletFirst = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 30,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFirst)
        val bulletSecond = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 50,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletSecond)
        val bulletThird = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 70,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletThird)
    }


    private fun initPlayerFourthWeapsonBullet() {
        val bulletFirst = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 50,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFirst)
        val bulletSecond = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet + 15f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(340f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 20,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletSecond)
        val bulletThird = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet * -1 - 15f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(20f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 80,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletThird)
    }


    private fun initPlayerFifthWeapsonBullet() {
        val bulletFirst = BulletFactory.createBullet(
                velocity = Vector2(0f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet),
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 50,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFirst)
        val bulletSecond = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet + 15f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(340f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 10,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletSecond)
        val bulletThird = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet * -1 - 15f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(20f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 90,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletThird)
        val bulletFourth = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet + 25f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(350f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 30,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFourth)
        val bulletFifth = BulletFactory.createBullet(
                velocity = Vector2(player.speedBullet * -1 - 25f, player.speedBullet),
                spriteBullet = Sprite(resource.playerBullet).apply {
                    setOrigin(resource.playerBullet.width / 2, resource.playerBullet.height / 2)
                    rotate(10f)
                },
                damage = player.damageBullet,
                position = Vector2(player.playerSpite.x + player.height / 100 * 70,
                        player.playerSpite.y - resource.playerBullet.height / 2)
        )
        gameObjectManager.addGameObject(bulletFifth)
    }


    private fun updateBullet(delta: Float) {
        allObj.forEach { entry ->
            val teamp = entry.value
            if (teamp is Bullet) {
                teamp.moving(delta)
                removeBulletIfUnderGameScreen(teamp)
            }
        }
    }


    private fun removeBulletIfUnderGameScreen(bullet: Bullet) {
        if (bullet.position.y < 0 - bullet.spriteBullet.height) {
            gameObjectManager.removeObject(bullet.idGameObject)
        }
    }
}
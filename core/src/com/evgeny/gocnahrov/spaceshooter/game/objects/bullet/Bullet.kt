package com.evgeny.gocnahrov.spaceshooter.game.objects.bullet

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.base.ObjectMoving

class Bullet(
        velocity: Vector2,
        val spriteBullet: Sprite,
        val damage: Int,
        val position: Vector2
) : ObjectMoving(
        velocity = velocity,
        acceleration = Vector2(0f, 0f),
        layer = Layer.FourthLayer
) {

    private val centerWidth = spriteBullet.width / 2
    private val centerHeight = spriteBullet.height / 2
    lateinit var rectangle: Rectangle

    init {
        spriteBullet.x = position.x - centerWidth
        spriteBullet.y = position.y - centerHeight
        rectangle = Rectangle(position.x - centerWidth, position.y - centerHeight, spriteBullet.width, spriteBullet.height)
    }


    fun moving(delta: Float) {
        position.x += velocity.x * delta
        position.y += velocity.y * delta
        spriteBullet.x = position.x - centerWidth
        spriteBullet.y = position.y - centerHeight
        rectangle.setPosition(position.x - centerWidth, position.y - centerHeight)
    }


    override fun render(delta: Float, batch: SpriteBatch) {
        spriteBullet.draw(batch)
    }


    override fun renderDebug(delta: Float, shapeRendererDebug: ShapeRenderer) {
        shapeRendererDebug.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height)
    }
}
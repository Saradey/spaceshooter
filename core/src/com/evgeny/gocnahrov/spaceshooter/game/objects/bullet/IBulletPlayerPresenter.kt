package com.evgeny.gocnahrov.spaceshooter.game.objects.bullet

interface IBulletPlayerPresenter {

    fun update(delta: Float)

}
package com.evgeny.gocnahrov.spaceshooter.game.objects.background

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.base.ObjectMoving

class BackgroundCosmos(
        velocity: Float,
        val position: Vector2,
        private val backgroundSprite: Sprite,
        val heightCosmos: Float,
        layer: Layer
) : ObjectMoving(Vector2(0f, velocity), Vector2(0f, 0f), layer) {


    override fun render(delta: Float, batch: SpriteBatch) {
        backgroundSprite.draw(batch)
    }


    fun setNewYPosition(y: Float) {
        backgroundSprite.y = y
        position.y = y
    }

    fun updatePosition(delta: Float) {
        position.y += velocity.y * delta
        backgroundSprite.y = position.y
    }
}
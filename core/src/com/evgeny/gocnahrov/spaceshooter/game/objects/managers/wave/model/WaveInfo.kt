package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model

import com.google.gson.annotations.SerializedName

data class WaveInfo(
        @SerializedName("name") var name: String,
        @SerializedName("intervalGenerateShips") var intervalGenerateShips: Float,
        @SerializedName("howManyShips") var howManyShips: Int,
        @SerializedName("easyShips") var easyShips: Int,
        @SerializedName("mediumShips") var mediumShips: Int,
        @SerializedName("hardShips") var hardShips: Int,
        @SerializedName("startWave") var startWave: Int,
        @SerializedName("isRepeatRoute") var isRepeatRoute: Boolean,
        @SerializedName("dots") var dots: List<Dots>,
        var starNexGenerateEnemy: Float = 0f,
        var valueNexGenerateEnemy: Float = 0f
)
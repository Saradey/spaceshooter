package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj

import com.evgeny.gocnahrov.spaceshooter.game.objects.base.GameObject
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.BackgroundCosmos
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.Bonus
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.Player
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KFunction0

interface IGameObjectManager {

    fun getAllGameObject(): ConcurrentHashMap<Int, GameObject>

    fun addGameObject(obj: GameObject)

    fun getBackgrounds(): List<BackgroundCosmos>

    fun getPlayer(): Player

    fun removeObject(idObj: Int)

}
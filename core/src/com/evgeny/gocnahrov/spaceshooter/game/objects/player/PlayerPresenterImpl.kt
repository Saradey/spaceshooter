package com.evgeny.gocnahrov.spaceshooter.game.objects.player

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.base.BaseInputHandler
import com.evgeny.gocnahrov.spaceshooter.common.ILogger
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.Player.Companion.DISTANCE_FROM_FINGER
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import javax.inject.Inject

class PlayerPresenterImpl @Inject constructor(
        private val logger: ILogger,
        private val objectManager: IGameObjectManager,
        private val windowManager: WindowManager
) : BaseInputHandler(), IPlayerPresenter {

    companion object {
        private const val BOARDING_ZONE_X = 10f
        private const val BOARDING_ZONE_Y_TOP = 20f
    }


    init {
        Gdx.input.inputProcessor = this
    }

    private val player: Player by lazy {
        objectManager.getPlayer()
    }

    private val clickPosition: Vector2 by lazy {
        Vector2(player.position.x, player.position.y)
    }

    private var distance = 0f
    private var boardingZoneX = windowManager.gameWidth - BOARDING_ZONE_X
    private var boardingZoneY = windowManager.gameHeight


    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        clickPosition.set(screenX.toFloat() * windowManager.howMatchSizeWidth,
                screenY.toFloat() * windowManager.howMatchSizeHeight)
        return super.touchDragged(screenX, screenY, pointer)
    }


    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        clickPosition.set(screenX.toFloat() * windowManager.howMatchSizeWidth,
                screenY.toFloat() * windowManager.howMatchSizeHeight)
        player.isMove = true
        logger.logD("x: ${clickPosition.x} y: ${clickPosition.y}")
        return super.touchDown(screenX, screenY, pointer, button)
    }


    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        player.isMove = false
        return super.touchUp(screenX, screenY, pointer, button)
    }


    override fun update(delta: Float) {
        if (player.isMove) {
            distance = player.position.dst(clickPosition)
            if (distance > 1f) {
                player.setNewPosition(
                        player.position.x + (player.velocityValue * delta * (clickPosition.x - player.position.x) / distance),
                        player.position.y + (player.velocityValue * delta * (clickPosition.y - player.position.y) / distance)
                )
                boardingZone()
            }
        }
    }


    private fun boardingZone() {
        when {
            boardingZoneX < player.position.x -> player.setNewPosition(boardingZoneX, player.position.y)
            player.position.x < BOARDING_ZONE_X -> player.setNewPosition(BOARDING_ZONE_X, player.position.y)
        }
        when {
            boardingZoneY < player.position.y -> player.setNewPosition(player.position.x, boardingZoneY)
            player.position.y < BOARDING_ZONE_Y_TOP -> player.setNewPosition(player.position.x, BOARDING_ZONE_Y_TOP)
        }
    }


}


package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave

import com.evgeny.gocnahrov.spaceshooter.common.ILogger
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init.IInitGameObject
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.LvlInfo
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.WaveInfo
import com.evgeny.gocnahrov.spaceshooter.managers.resource.ResourceImageGameManager
import com.google.gson.Gson
import javax.inject.Inject

class WaveManagerImpl @Inject constructor(
        private val resourceManager: ResourceImageGameManager,
        private val initGameObject: IInitGameObject,
        private val logger: ILogger
) : IWaveManager {

    private var lvlNow = 0
    private var gsonParser = Gson()
    private lateinit var lvlInfo: LvlInfo
    private var nextWave = 0
    private var waves: MutableList<WaveInfo> = mutableListOf()


    override fun activateLvlWave(lvlStartNow: Int) {
        lvlNow = lvlStartNow
        val fileLvl = resourceManager.lvlsInfo[lvlStartNow]
        val info = fileLvl.readString()
        lvlInfo = gsonParser.fromJson(info, LvlInfo::class.java)
    }


    override fun getLvlInfo(): LvlInfo {
        return lvlInfo
    }


    override fun generateNextWave() {
        if (lvlInfo.waves.size > nextWave) {
            waves.add(lvlInfo.waves[nextWave])
            waves[nextWave].starNexGenerateEnemy = waves[nextWave].intervalGenerateShips
            nextWave++
        }
    }


    override fun update(delta: Float) {
        updateAllWaves(delta)
    }


    private fun updateAllWaves(delta: Float) {
        waves.forEach { wave ->
            wave.valueNexGenerateEnemy += delta
            if (wave.howManyShips > 0) {
                if (wave.valueNexGenerateEnemy > wave.starNexGenerateEnemy) {
                    wave.starNexGenerateEnemy += wave.intervalGenerateShips
                    generateEnemy(wave)
                }
            }
        }
    }


    private fun generateEnemy(wave: WaveInfo) {
        wave.howManyShips--
        when {
            wave.easyShips > 0 -> {
                wave.easyShips--
                initGameObject.initEasyEnemy(wave)
            }
            wave.mediumShips > 0 -> {
                wave.mediumShips--
                initGameObject.initMediumEnemy(wave)
            }
            wave.hardShips > 0 -> {
                wave.hardShips--
                initGameObject.initHardEnemy(wave)
            }
        }
    }
}
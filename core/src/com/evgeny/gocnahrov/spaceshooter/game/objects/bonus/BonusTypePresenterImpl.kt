package com.evgeny.gocnahrov.spaceshooter.game.objects.bonus

import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import javax.inject.Inject

class BonusTypePresenterImpl @Inject constructor(
        private val objectManager: IGameObjectManager,
        private val windowManager: WindowManager
) : IBonusTypePresenter {

    private val allObject = objectManager.getAllGameObject()


    override fun update(delta: Float) {
        allObject.forEach { entry ->
            val teamp = entry.value
            if (teamp is Bonus) {
                teamp.moving(delta)
                ifBonusUnderGameScreenThenDeleteObj(teamp)
            }
        }
    }


    private fun ifBonusUnderGameScreenThenDeleteObj(bonus: Bonus) {
        if (bonus.position.y > windowManager.gameHeight + bonus.spriteBonus.height) {
            objectManager.removeObject(bonus.idGameObject)
        }
    }

}
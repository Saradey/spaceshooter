package com.evgeny.gocnahrov.spaceshooter.game.objects.player

interface IPlayerPresenter {

    fun update(delta: Float)

}
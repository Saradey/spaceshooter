package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave

import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.LvlInfo

interface IWaveManager {

    fun activateLvlWave(lvlStartNow: Int)

    fun getLvlInfo(): LvlInfo

    fun update(delta: Float)

    fun generateNextWave()

}
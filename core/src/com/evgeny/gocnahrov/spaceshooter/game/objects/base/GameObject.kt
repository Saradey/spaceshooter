package com.evgeny.gocnahrov.spaceshooter.game.objects.base

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import kotlin.random.Random

abstract class GameObject(val layer: Layer) {
    val idGameObject = Random.nextInt(0, 1_000_000)
    abstract fun render(delta: Float, batch: SpriteBatch)
    open fun renderDebug(delta: Float, shapeRendererDebug: ShapeRenderer) {}
}
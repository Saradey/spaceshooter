package com.evgeny.gocnahrov.spaceshooter.game.objects.enemy

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.base.ObjectMoving

class Enemy(
        velocity: Vector2,
        val enemySprite: Sprite,
        val position: Vector2,
        val isFire: Boolean
) : ObjectMoving(
        acceleration = Vector2(0f, 0f),
        layer = Layer.ThirdLayer,
        velocity = velocity
) {


    override fun render(delta: Float, batch: SpriteBatch) {
        
    }

    override fun renderDebug(delta: Float, shapeRendererDebug: ShapeRenderer) {

    }
}
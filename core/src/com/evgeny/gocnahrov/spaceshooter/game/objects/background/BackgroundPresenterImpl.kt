package com.evgeny.gocnahrov.spaceshooter.game.objects.background

import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import javax.inject.Inject

class BackgroundPresenterImpl @Inject constructor(
        private val objectManager: IGameObjectManager
) : IBackgroundPresenter {

    private val objectBackgrounds: List<BackgroundCosmos> by lazy {
        objectManager.getBackgrounds()
    }


    override fun update(delta: Float) {
        objectBackgrounds.forEach { background ->
            background.updatePosition(delta)
            calculatePositionFirstBack(background)
        }
    }


    private fun calculatePositionFirstBack(background: BackgroundCosmos) {
        if (background.position.y > background.heightCosmos) {
            background.setNewYPosition(0f - (background.heightCosmos))
        }
    }

}
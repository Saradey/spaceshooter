package com.evgeny.gocnahrov.spaceshooter.game.objects.player

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2

class PlayerFactory {

    companion object {
        fun createPlayer(playerSpite: Sprite,
                         position: Vector2,
                         velocityValue: Float,
                         weapsonLvl: Int,
                         hp: Int,
                         fasterShoot: Float,
                         speedBullet: Float,
                         damageBullet: Int): Player {
            return Player(
                    playerSpite,
                    position,
                    Vector2(0f, 0f),
                    velocityValue,
                    weapsonLvl,
                    hp,
                    fasterShoot,
                    speedBullet,
                    damageBullet
            )
        }
    }

}
package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init

import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.WaveInfo

interface IInitGameObject {

    fun initBackground()

    fun initPlayer()

    fun initBonus()

    fun initEasyEnemy(wave: WaveInfo)

    fun initMediumEnemy(wave: WaveInfo)

    fun initHardEnemy(wave: WaveInfo)

}
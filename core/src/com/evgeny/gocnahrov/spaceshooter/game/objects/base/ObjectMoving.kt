package com.evgeny.gocnahrov.spaceshooter.game.objects.base

import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer

abstract class ObjectMoving(
        var velocity: Vector2,
        protected val acceleration: Vector2,
        layer: Layer
) : GameObject(layer)
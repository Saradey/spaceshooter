package com.evgeny.gocnahrov.spaceshooter.game.objects.player

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.base.ObjectMoving

/**
 * @param fasterShoot скорость стрельбы в секундах
 */
class Player(
        val playerSpite: Sprite,
        val position: Vector2,
        velocity: Vector2,
        val velocityValue: Float,
        var weapsonLvl: Int,
        private var hp: Int,
        val fasterShoot: Float,
        val speedBullet: Float,
        val damageBullet: Int
) : ObjectMoving(
        velocity = velocity,
        acceleration = Vector2(0f, 0f),
        layer = Layer.ThirdLayer
) {

    companion object {
        const val DISTANCE_FROM_FINGER = 12f
        const val MAX_WEAPSON_LVL = 5
        const val MAX_HP = 3
    }

    var isMove = false
    var width = playerSpite.width
    var height = playerSpite.height
    var centerWidth = playerSpite.width / 2
    var centerHeight = playerSpite.height / 2
    private val radiusCircle = playerSpite.height / 2
    lateinit var areaObj: Circle


    init {
        playerSpite.setPosition(
                position.x - centerWidth,
                position.y - centerHeight - DISTANCE_FROM_FINGER
        )
        areaObj = Circle(playerSpite.x + centerWidth, playerSpite.y + centerHeight, radiusCircle)
    }


    override fun render(delta: Float, batch: SpriteBatch) {
        playerSpite.draw(batch)
    }


    override fun renderDebug(delta: Float, shapeRendererDebug: ShapeRenderer) {
        shapeRendererDebug.circle(areaObj.x, areaObj.y, radiusCircle)
    }


    fun setNewPosition(x: Float, y: Float) {
        position.set(x, y)
        playerSpite.setPosition(position.x - centerWidth, (position.y - centerHeight) - DISTANCE_FROM_FINGER)
        areaObj.setPosition(playerSpite.x + centerWidth, playerSpite.y + centerHeight)
    }


    fun addWeapson() {
        if (weapsonLvl < MAX_WEAPSON_LVL) {
            weapsonLvl++
        }
    }


    fun generateShield() {

    }


    fun incrementHp() {
        if (hp < MAX_HP) {
            hp++
        }
    }


}
package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model

import com.google.gson.annotations.SerializedName

data class LvlInfo(
        @SerializedName("isBoss") var isBoss: Boolean,
        @SerializedName("nextBonus") var nextBonus: Long,
        @SerializedName("nextWaveGenerate") var nextWaveGenerate: Float,
        @SerializedName("waves") var waves: List<WaveInfo>
)
package com.evgeny.gocnahrov.spaceshooter.game.objects.bonus

interface IBonusTypePresenter {

    fun update(delta: Float)

}
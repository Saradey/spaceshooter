package com.evgeny.gocnahrov.spaceshooter.game.objects.bullet

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2

class BulletFactory {
    companion object {
        fun createBullet(
                velocity: Vector2,
                spriteBullet: Sprite,
                damage: Int,
                position: Vector2
        ): Bullet {
            return Bullet(
                    velocity,
                    spriteBullet,
                    damage,
                    position
            )
        }
    }
}
package com.evgeny.gocnahrov.spaceshooter.game.objects.background

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.Layer

class BackgroundFactory {
    companion object {
        fun instantiateBackground(
                velocity: Float,
                position: Vector2,
                backgroundSprite: Sprite,
                heightCosmos: Float,
                layer: Layer
        ): BackgroundCosmos {
            return BackgroundCosmos(
                    velocity,
                    position,
                    backgroundSprite,
                    heightCosmos,
                    layer
            )
        }
    }
}
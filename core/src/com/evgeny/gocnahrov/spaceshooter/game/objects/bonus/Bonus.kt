package com.evgeny.gocnahrov.spaceshooter.game.objects.bonus

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.consts.DEBUG
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.base.ObjectMoving

class Bonus(
        private val velocityValue: Float,
        val spriteBonus: Sprite,
        val position: Vector2,
        val bonusType: BonusType
) : ObjectMoving(
        velocity = Vector2(0f, velocityValue),
        acceleration = Vector2(0f, 0f),
        layer = Layer.ThirdLayer
) {
    lateinit var rectangle: Rectangle


    init {
        spriteBonus.setPosition(position.x, position.y)
        rectangle = Rectangle(position.x, position.y, spriteBonus.width, spriteBonus.height)
    }


    override fun render(delta: Float, batch: SpriteBatch) {
        spriteBonus.draw(batch)
    }


    override fun renderDebug(delta: Float, shapeRendererDebug: ShapeRenderer) {
        shapeRendererDebug.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height)
    }


    fun moving(delta: Float) {
        position.y += velocity.y * delta
        spriteBonus.y = position.y
        rectangle.setPosition(spriteBonus.x, spriteBonus.y)
    }

}
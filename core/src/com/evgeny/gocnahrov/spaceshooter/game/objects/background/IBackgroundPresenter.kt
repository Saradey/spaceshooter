package com.evgeny.gocnahrov.spaceshooter.game.objects.background

interface IBackgroundPresenter {

    fun update(delta: Float)

}
package com.evgeny.gocnahrov.spaceshooter.game.objects.enemy

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2

class EnemyFactory {
    companion object {
        fun createEnemy(velocity: Vector2,
                        enemySprite: Sprite,
                        position: Vector2,
                        isFire: Boolean): Enemy {
            return Enemy(
                    velocity = velocity,
                    enemySprite = enemySprite,
                    position = position,
                    isFire = isFire
            )
        }

    }
}
package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model

import com.google.gson.annotations.SerializedName

data class Dots(
        @SerializedName("x") var x: Float,
        @SerializedName("y") var y: Float
)
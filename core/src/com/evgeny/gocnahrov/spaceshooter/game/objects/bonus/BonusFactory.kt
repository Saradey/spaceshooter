package com.evgeny.gocnahrov.spaceshooter.game.objects.bonus

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2

class BonusFactory {
    companion object {
        fun crateNewBonus(velocityValue: Float,
                          spriteBonus: Sprite,
                          position: Vector2,
                          bonusType: BonusType): Bonus {
            return Bonus(
                    velocityValue,
                    spriteBonus,
                    position,
                    bonusType
            )
        }
    }
}
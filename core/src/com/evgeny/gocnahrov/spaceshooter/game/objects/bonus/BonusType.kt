package com.evgeny.gocnahrov.spaceshooter.game.objects.bonus

enum class BonusType constructor(val idType: Int) {
    HP(0),
    SHIELD(1),
    WEAPSONS(2)
}
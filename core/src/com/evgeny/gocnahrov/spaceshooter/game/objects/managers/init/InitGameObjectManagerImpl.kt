package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import com.evgeny.gocnahrov.spaceshooter.common.ILogger
import com.evgeny.gocnahrov.spaceshooter.consts.*
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.BackgroundFactory
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.BonusFactory
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.BonusType
import com.evgeny.gocnahrov.spaceshooter.game.objects.enemy.EnemyFactory
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.WaveInfo
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.PlayerFactory
import com.evgeny.gocnahrov.spaceshooter.managers.resource.ResourceImageGameManager
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import javax.inject.Inject
import kotlin.random.Random

class InitGameObjectManagerImpl @Inject constructor(
        private val resourceManager: ResourceImageGameManager,
        private val gameObjectManager: IGameObjectManager,
        private val windowManager: WindowManager,
        private val logger: ILogger
) : IInitGameObject {


    override fun initBackground() {
        val firstBackgroundCosmos = BackgroundFactory.instantiateBackground(
                velocity = VELOCITY_OBJ_COSMOS,
                position = Vector2(0f, 0f),
                backgroundSprite = resourceManager.backgroundCosmos2,
                heightCosmos = resourceManager.backgroundCosmos2.height,
                layer = Layer.FirstLayer
        )
        gameObjectManager.addGameObject(firstBackgroundCosmos)
        val secondBackgroundCosmos = BackgroundFactory.instantiateBackground(
                velocity = VELOCITY_OBJ_COSMOS,
                position = Vector2(0f, 0f - firstBackgroundCosmos.heightCosmos),
                backgroundSprite = resourceManager.backgroundCosmos1,
                heightCosmos = resourceManager.backgroundCosmos1.height,
                layer = Layer.FirstLayer
        )
        gameObjectManager.addGameObject(secondBackgroundCosmos)
        val firstBackgroundStar = BackgroundFactory.instantiateBackground(
                velocity = VELOCITY_OBJ_STAR,
                position = Vector2(0f, 0f),
                backgroundSprite = resourceManager.backgroundStar1,
                heightCosmos = resourceManager.backgroundStar1.height,
                layer = Layer.SecondLayer
        )
        gameObjectManager.addGameObject(firstBackgroundStar)
        val secondBackgroundStar = BackgroundFactory.instantiateBackground(
                velocity = VELOCITY_OBJ_STAR,
                position = Vector2(0f, 0f - firstBackgroundCosmos.heightCosmos),
                backgroundSprite = resourceManager.backgroundStar2,
                heightCosmos = resourceManager.backgroundStar2.height,
                layer = Layer.SecondLayer
        )
        gameObjectManager.addGameObject(secondBackgroundStar)
    }


    override fun initPlayer() {
        val player = PlayerFactory.createPlayer(
                playerSpite = resourceManager.playerShip,
                position = Vector2(
                        windowManager.gameDotCenterX,
                        windowManager.gameHeight / 100 * START_PLAYER_TO_BACKGROUND_PERCENT_VALUE
                ),
                velocityValue = VELOCITY_OBJ_PLAYER,
                weapsonLvl = 1,
                hp = 2,
                fasterShoot = 0.5f,
                speedBullet = -35f,
                damageBullet = 1
        )
        gameObjectManager.addGameObject(player)
    }


    override fun initBonus() {
        val nextBonus = Random.nextInt(0, 3)
        val bonus = BonusFactory.crateNewBonus(
                velocityValue = VELOCITY_OBJ_BONUS,
                spriteBonus = getSpriteBonusType(nextBonus),
                position = getRandomPosition(),
                bonusType = getBonusType(nextBonus)
        )
        gameObjectManager.addGameObject(bonus)
    }


    private fun getBonusType(nextBonus: Int): BonusType {
        return when (nextBonus) {
            0 -> BonusType.HP
            1 -> BonusType.SHIELD
            else -> BonusType.WEAPSONS
        }
    }


    private fun getSpriteBonusType(nextBonus: Int): Sprite {
        return when (nextBonus) {
            BonusType.WEAPSONS.idType -> Sprite(resourceManager.weapsonBonus)
            BonusType.HP.idType -> Sprite(resourceManager.hpBonus)
            else -> Sprite(resourceManager.shieldBonus)
        }
    }


    private fun getRandomPosition(): Vector2 {
        val xPosition = Random.nextInt((0 + resourceManager.shieldBonus.width / 2 + BOARD_OBJ_TO_BONUS).toInt(),
                (windowManager.gameWidth.toInt() - resourceManager.shieldBonus.width / 2 - BOARD_OBJ_TO_BONUS).toInt()).toFloat()
        return Vector2(
                xPosition,
                0f - resourceManager.shieldBonus.height
        )
    }


    override fun initEasyEnemy(wave: WaveInfo) {
        val easyEnemy = EnemyFactory.createEnemy(
                velocity = Vector2(VELOCITY_ENEMY_EASY, VELOCITY_ENEMY_EASY),
                enemySprite = resourceManager.enemyEasy,
                position = Vector2(wave.dots.first().x, wave.dots.first().y),
                isFire = false
        )
        gameObjectManager.addGameObject(easyEnemy)
    }


    override fun initMediumEnemy(wave: WaveInfo) {

    }


    override fun initHardEnemy(wave: WaveInfo) {

    }
}
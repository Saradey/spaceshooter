package com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj

import com.evgeny.gocnahrov.spaceshooter.game.objects.base.GameObject
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.BackgroundCosmos
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.Bonus
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.Player
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject

class GameObjectManagerImpl @Inject constructor() : IGameObjectManager {

    private val gameObject = ConcurrentHashMap<Int, GameObject>()


    override fun getAllGameObject(): ConcurrentHashMap<Int, GameObject> {
        return gameObject
    }


    override fun addGameObject(obj: GameObject) {
        gameObject[obj.idGameObject] = obj
    }


    override fun getBackgrounds(): List<BackgroundCosmos> {
        return gameObject.toList().map { pair ->
            pair.second
        }.filterIsInstance<BackgroundCosmos>()
    }


    override fun getPlayer(): Player {
        return gameObject.filter { entry ->
            entry.value is Player
        }.toList()
                .map {
                    it.second as Player
                }
                .first()
    }


    override fun removeObject(idObj: Int) {
        gameObject.remove(idObj)
    }
}
package com.evgeny.gocnahrov.spaceshooter.base

import com.badlogic.gdx.Screen

abstract class BaseScreen : Screen {
    override fun hide() {}
    override fun show() {}
    override fun render(delta: Float) {}
    override fun pause() {}
    override fun resume() {}
    override fun resize(width: Int, height: Int) {}
    override fun dispose() {}
}
package com.evgeny.gocnahrov.spaceshooter

import com.badlogic.gdx.Game
import com.badlogic.gdx.Screen
import com.evgeny.gocnahrov.spaceshooter.consts.DEBUG
import com.evgeny.gocnahrov.spaceshooter.consts.TEST_SCREEN
import com.evgeny.gocnahrov.spaceshooter.di.components.ApplicationComponent
import com.evgeny.gocnahrov.spaceshooter.managers.navigation.INavigationManager
import com.evgeny.gocnahrov.spaceshooter.managers.navigation.ISwitcherScreen
import javax.inject.Inject


class App : Game(), ISwitcherScreen {

    @Inject
    lateinit var navigationManager: INavigationManager


    override fun create() {
        ApplicationComponent.init(this, this).inject(this)
        navigationManager.startStartScreen(TEST_SCREEN)
    }


    override fun showScreenNow(screen: Screen) {
        setScreen(screen)
    }

}
package com.evgeny.gocnahrov.spaceshooter.managers.resource

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.evgeny.gocnahrov.spaceshooter.di.components.ApplicationComponent
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import java.util.*
import javax.inject.Inject

class ResourceImageGameManager {

    private lateinit var atlas: TextureAtlas
    private lateinit var bonusAtlas: TextureAtlas
    lateinit var enemyBoss: Sprite
    lateinit var enemyBullet: Sprite
    lateinit var enemyEasy: Sprite
    lateinit var enemyHard1: Sprite
    lateinit var enemyHard2: Sprite
    lateinit var playerBullet: Sprite
    lateinit var playerShield: Sprite
    lateinit var playerShip: Sprite
    lateinit var backgroundCosmos1: Sprite
    lateinit var backgroundCosmos2: Sprite
    lateinit var backgroundStar1: Sprite
    lateinit var backgroundStar2: Sprite
    lateinit var hpBonus: Sprite
    lateinit var shieldBonus: Sprite
    lateinit var weapsonBonus: Sprite
    lateinit var lvlsInfo: MutableList<FileHandle>


    @Inject
    lateinit var windowManager: WindowManager

    init {
        ApplicationComponent.component.inject(this)
        load()
    }


    private fun load() {
        atlas = TextureAtlas(Gdx.files.internal("image/GameObjectAtlas.atlas"), true)
        enemyBoss = Sprite(atlas.findRegion("enemyBoss"))
        resizeSprite(enemyBoss)
        enemyBullet = Sprite(atlas.findRegion("enemyBullet"))
        resizeSprite(enemyBullet)
        enemyEasy = Sprite(atlas.findRegion("enemyEasy"))
        resizeSprite(enemyEasy)
        enemyHard1 = Sprite(atlas.findRegion("enemyHard1"))
        resizeSprite(enemyHard1)
        enemyHard2 = Sprite(atlas.findRegion("enemyHard2"))
        resizeSprite(enemyHard2)
        playerBullet = Sprite(atlas.findRegion("playerBullet"))
        resizePlayerBullet(playerBullet)
        playerShield = Sprite(atlas.findRegion("playerShield"))
        resizeSprite(playerShield)
        playerShip = Sprite(atlas.findRegion("playerShip"))
        resizeSprite(playerShip)
        backgroundCosmos1 = Sprite(atlas.findRegion("backgroundCosmos1"))
        resizeBackground(backgroundCosmos1)
        backgroundCosmos2 = Sprite(atlas.findRegion("backgroundCosmos2"))
        resizeBackground(backgroundCosmos2)
        backgroundStar1 = Sprite(atlas.findRegion("backgroundStar1"))
        resizeBackground(backgroundStar1)
        backgroundStar2 = Sprite(atlas.findRegion("backgroundStar2"))
        resizeBackground(backgroundStar2)
        bonusAtlas = TextureAtlas(Gdx.files.internal("image/Bonus.atlas"), true)
        hpBonus = Sprite(bonusAtlas.findRegion("hpBonus"))
        resizeSprite(hpBonus)
        shieldBonus = Sprite(bonusAtlas.findRegion("shieldBonus"))
        resizeSprite(shieldBonus)
        weapsonBonus = Sprite(bonusAtlas.findRegion("weapsonBonus"))
        resizeSprite(weapsonBonus)
        loadFileInfo()
    }


    private fun loadFileInfo() {
        val files = Gdx.files.internal("lvls/").list()
        lvlsInfo = files.toMutableList()
    }


    private fun resizeBackground(sprite: Sprite) {
        val scale = windowManager.gameWidth / sprite.width
        var newWidth = sprite.width * scale
        var newHeight = sprite.height * scale
        if (newHeight < windowManager.gameHeight) {
            newWidth += windowManager.gameHeight - newHeight
            newHeight += windowManager.gameHeight - newHeight
        }
        sprite.setSize(newWidth, newHeight)
    }


    private fun resizeSprite(sprite: Sprite) {
        val scale = 0.08f
        sprite.setSize(sprite.width * scale, sprite.height * scale)
    }


    private fun resizePlayerBullet(sprite: Sprite) {
        val scale = 0.04f
        sprite.setSize(sprite.width * scale, sprite.height * scale)
    }


    fun dispose() {
        atlas.dispose()
    }

}
package com.evgeny.gocnahrov.spaceshooter.managers.navigation

import com.evgeny.gocnahrov.spaceshooter.screens.game.GameScreen
import javax.inject.Inject

class NavigationManagerImpl @Inject constructor(
        private val switcherScreen: ISwitcherScreen
) : INavigationManager {


    override fun startStartScreen(debugOn: Boolean) {
        if (debugOn) {
        } else {
            switcherScreen.showScreenNow(GameScreen())
        }
    }


}
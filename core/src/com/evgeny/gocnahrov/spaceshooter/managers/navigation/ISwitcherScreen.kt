package com.evgeny.gocnahrov.spaceshooter.managers.navigation

import com.badlogic.gdx.Screen

interface ISwitcherScreen {

    fun showScreenNow(screen: Screen)

}
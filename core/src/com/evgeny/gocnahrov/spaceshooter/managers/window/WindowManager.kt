package com.evgeny.gocnahrov.spaceshooter.managers.window

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

class WindowManager {

    var gameWidth = 0f
    var gameHeight = 0f

    var widthScreen = 0f
    var heightScreen = 0f

    var gameDotCenterX = 0f
    var gameDotCenterY = 0f

    var howMatchSizeWidth = 0f
    var howMatchSizeHeight = 0f

    var batch = SpriteBatch()
    var orthographicCamera = OrthographicCamera()
    var shapeRendererDebug = ShapeRenderer()


    init {
        widthScreen = Gdx.graphics.width.toFloat()
        heightScreen = Gdx.graphics.height.toFloat()
        gameWidth = widthScreen / (heightScreen / 120f)
        gameHeight = heightScreen / (widthScreen / gameWidth)
        gameDotCenterX = gameWidth / 2f
        gameDotCenterY = gameHeight / 2f
        howMatchSizeWidth = gameWidth / widthScreen
        howMatchSizeHeight = gameHeight / heightScreen
    }


    fun getSpriteBatch(): SpriteBatch {
        val camera = getCamera()
        batch = SpriteBatch()
        batch.projectionMatrix = camera.combined
        return batch
    }


    private fun getCamera(): OrthographicCamera {
        orthographicCamera = OrthographicCamera()
        orthographicCamera.setToOrtho(true, gameWidth, gameHeight)
        return orthographicCamera
    }


    fun getShapeRenderer(): ShapeRenderer {
        shapeRendererDebug.projectionMatrix = orthographicCamera.combined
        return shapeRendererDebug
    }

}
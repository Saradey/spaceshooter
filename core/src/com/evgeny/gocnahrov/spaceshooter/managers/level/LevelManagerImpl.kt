package com.evgeny.gocnahrov.spaceshooter.managers.level

import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init.IInitGameObject
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.IWaveManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.wave.model.LvlInfo
import java.util.*
import javax.inject.Inject

class LevelManagerImpl @Inject constructor(
        private val initObj: IInitGameObject,
        private val waveManager: IWaveManager
) : ILevelManager {

    private val timerScheduler = Timer()
    private val lvlStartNow = 0
    private lateinit var lvlInfo: LvlInfo
    private var diapasonBonusGenerate = 0L
    private var startWaveGenerate = 0f
    private var valueWaveGenerate = 0f


    override fun initBeginObject() {
        initObj.initBackground()
        initObj.initPlayer()
        waveManager.activateLvlWave(lvlStartNow)
        lvlInfo = waveManager.getLvlInfo()
        initParameterLvl()
        initTimerBonusGenerate()
    }


    private fun initParameterLvl() {
        diapasonBonusGenerate = lvlInfo.nextBonus
        startWaveGenerate = lvlInfo.nextWaveGenerate
    }


    private fun initTimerBonusGenerate() {
        val timer = object : TimerTask() {
            override fun run() {
                initObj.initBonus()
            }
        }
        timerScheduler.schedule(timer, diapasonBonusGenerate, diapasonBonusGenerate)
    }


    override fun update(delta: Float) {
        newWaveGenerate(delta)
        waveManager.update(delta)
    }


    private fun newWaveGenerate(delta: Float) {
        valueWaveGenerate += delta
        if (valueWaveGenerate > startWaveGenerate) {
            startWaveGenerate += lvlInfo.nextWaveGenerate
            waveManager.generateNextWave()
        }
    }


}
package com.evgeny.gocnahrov.spaceshooter.managers.level

interface ILevelManager {

    fun initBeginObject()

    fun update(delta: Float)

}
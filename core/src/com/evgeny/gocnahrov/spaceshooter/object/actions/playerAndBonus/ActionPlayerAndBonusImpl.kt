package com.evgeny.gocnahrov.spaceshooter.`object`.actions.playerAndBonus

import com.badlogic.gdx.math.Intersector
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.Bonus
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.BonusType
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.Player
import javax.inject.Inject

class ActionPlayerAndBonusImpl @Inject constructor(
        private val gameObjectManager: IGameObjectManager
) : IActionPlayerAndBonus {

    private val player: Player by lazy {
        gameObjectManager.getPlayer()
    }

    private val allObject = gameObjectManager.getAllGameObject()


    override fun update() {
        allObject.forEach { entry ->
            val teamp = entry.value
            if (teamp is Bonus) {
                if (Intersector.overlaps(player.areaObj, teamp.rectangle)) {
                    collisionPlayerAndDoIt(teamp, entry.key)
                }
            }
        }
    }


    private fun collisionPlayerAndDoIt(teamp: Bonus, idObj: Int) {
        when (teamp.bonusType) {
            BonusType.WEAPSONS -> player.addWeapson()
            BonusType.SHIELD -> player.generateShield()
            BonusType.HP -> player.incrementHp()
        }
        gameObjectManager.removeObject(idObj)
    }

}
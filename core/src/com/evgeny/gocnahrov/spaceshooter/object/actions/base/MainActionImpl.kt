package com.evgeny.gocnahrov.spaceshooter.`object`.actions.base

import com.evgeny.gocnahrov.spaceshooter.`object`.actions.playerAndBonus.IActionPlayerAndBonus
import javax.inject.Inject

class MainActionImpl @Inject constructor(
        private val playerAndBonus: IActionPlayerAndBonus
) : IMainAction {


    override fun update() {
        playerAndBonus.update()
    }


}
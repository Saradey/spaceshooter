package com.evgeny.gocnahrov.spaceshooter.screens.game.world

interface IGameWorld {

    fun update(delta: Float)

}
package com.evgeny.gocnahrov.spaceshooter.screens.game.render

interface IGameRender {

    fun render(delta: Float)

}
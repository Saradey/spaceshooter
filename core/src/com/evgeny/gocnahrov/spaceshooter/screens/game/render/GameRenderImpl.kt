package com.evgeny.gocnahrov.spaceshooter.screens.game.render

import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.evgeny.gocnahrov.spaceshooter.consts.DEBUG
import com.evgeny.gocnahrov.spaceshooter.consts.Layer
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.obj.IGameObjectManager
import com.evgeny.gocnahrov.spaceshooter.managers.window.WindowManager
import javax.inject.Inject

class GameRenderImpl @Inject constructor(
        windowManager: WindowManager,
        objectManager: IGameObjectManager
) : IGameRender {

    private val batch = windowManager.getSpriteBatch()
    private val shapeRendererDebug = windowManager.getShapeRenderer()
    private val gameObjects = objectManager.getAllGameObject()


    override fun render(delta: Float) {
        batch.begin()
        drawFirsLayer(delta)
        drawSecondLayer(delta)
        drawThirdLayer(delta)
        drawFourthLayer(delta)
        batch.end()
        shapeRendererDebug.begin(ShapeRenderer.ShapeType.Line)
        drawDebug(delta)
        shapeRendererDebug.end()
    }


    private fun drawDebug(delta: Float) {
        if (DEBUG) {
            gameObjects.forEach { entry ->
                entry.value.renderDebug(delta, shapeRendererDebug)
            }
        }
    }


    private fun drawFourthLayer(delta: Float) {
        gameObjects.forEach { entry ->
            if (entry.value.layer == Layer.FourthLayer) {
                entry.value.render(delta, batch)
            }
        }
    }


    private fun drawThirdLayer(delta: Float) {
        gameObjects.forEach { entry ->
            if (entry.value.layer == Layer.ThirdLayer) {
                entry.value.render(delta, batch)
            }
        }
    }


    private fun drawFirsLayer(delta: Float) {
        batch.disableBlending()
        gameObjects.forEach { entry ->
            if (entry.value.layer == Layer.FirstLayer) {
                entry.value.render(delta, batch)
            }
        }
    }


    private fun drawSecondLayer(delta: Float) {
        batch.enableBlending()
        gameObjects.forEach { entry ->
            if (entry.value.layer == Layer.SecondLayer) {
                entry.value.render(delta, batch)
            }
        }
    }

}
package com.evgeny.gocnahrov.spaceshooter.screens.game.world

import com.evgeny.gocnahrov.spaceshooter.`object`.actions.base.IMainAction
import com.evgeny.gocnahrov.spaceshooter.game.objects.background.IBackgroundPresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.bonus.IBonusTypePresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.bullet.IBulletPlayerPresenter
import com.evgeny.gocnahrov.spaceshooter.game.objects.player.IPlayerPresenter
import com.evgeny.gocnahrov.spaceshooter.managers.level.ILevelManager
import javax.inject.Inject

class GameWorldImpl @Inject constructor(
        private val backgroundPresenter: IBackgroundPresenter,
        private val playerPresenter: IPlayerPresenter,
        private val levelManager: ILevelManager,
        private val bonusPresenter: IBonusTypePresenter,
        private val mainAction: IMainAction,
        private val bulletPlayerPresenter: IBulletPlayerPresenter
) : IGameWorld {

    init {
        levelManager.initBeginObject()
    }

    override fun update(delta: Float) {
        backgroundPresenter.update(delta)
        playerPresenter.update(delta)
        bonusPresenter.update(delta)
        levelManager.update(delta)
        mainAction.update()
        bulletPlayerPresenter.update(delta)
    }
}
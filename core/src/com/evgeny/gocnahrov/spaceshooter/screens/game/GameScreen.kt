package com.evgeny.gocnahrov.spaceshooter.screens.game

import com.evgeny.gocnahrov.spaceshooter.base.BaseScreen
import com.evgeny.gocnahrov.spaceshooter.di.components.ApplicationComponent
import com.evgeny.gocnahrov.spaceshooter.game.objects.managers.init.IInitGameObject
import com.evgeny.gocnahrov.spaceshooter.screens.game.render.IGameRender
import com.evgeny.gocnahrov.spaceshooter.screens.game.world.IGameWorld
import javax.inject.Inject

class GameScreen : BaseScreen() {

    @Inject
    lateinit var gameWorld: IGameWorld

    @Inject
    lateinit var gameRender: IGameRender


    override fun show() {
        ApplicationComponent.component.inject(this)
    }


    override fun render(delta: Float) {
        gameRender.render(delta)
        gameWorld.update(delta)
    }
}
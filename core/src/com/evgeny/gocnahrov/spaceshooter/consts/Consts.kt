package com.evgeny.gocnahrov.spaceshooter.consts

const val DEBUG = true
const val TEST_SCREEN = false

const val VELOCITY_OBJ_BONUS = 12f
const val VELOCITY_OBJ_PLAYER = 15f
const val VELOCITY_OBJ_COSMOS = 15f
const val VELOCITY_OBJ_STAR = 10f
const val VELOCITY_ENEMY_EASY = 12f

const val BOARD_OBJ_TO_BONUS = 3f

const val START_PLAYER_TO_BACKGROUND_PERCENT_VALUE = 90
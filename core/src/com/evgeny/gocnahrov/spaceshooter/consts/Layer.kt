package com.evgeny.gocnahrov.spaceshooter.consts

enum class Layer {
    FirstLayer,
    SecondLayer,
    ThirdLayer,
    FourthLayer
}